///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

int main(){
   double diff;
   long sec, yr, syear = 31536000; 
   long dy, sday = 86400;
   long hr, shour = 3600;
   long min, smin = 60;
   struct tm refTime;

//   refTime.tm_sec = 0;
//   refTime.tm_min = 0;
   refTime.tm_hour = 4;
   refTime.tm_mday = 2;
   refTime.tm_mon = 5;
   refTime.tm_year = 100;
   refTime.tm_wday = 5;

   time_t ref = mktime(&refTime);

   printf("Reference Time:%s", asctime(&refTime));

   while(1){
      time_t t = time(NULL);
      diff = difftime(t,ref);
      yr = diff / syear;
      diff = diff - (yr * syear);
      dy = diff/sday;
      diff = diff - (dy * sday);
      hr =  diff/shour;
      diff = diff - (hr * shour);
      min = diff/smin;
      sec = diff - (min * smin);
      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",yr, dy, hr, min, sec);
      sleep(1);
   }
   return 0;

}
